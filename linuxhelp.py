#!/usr/bin/env python
#
# linuxhelp.py - tools for automating tasks related
# to maintaining a self-compiled kernel installation

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import os
import os.path
import subprocess
import sys

def line1_of_file_at(path):
    with open(path, "r") as f:
        return f.readline().strip()

def ensure_unused_path(path, description):
    if os.path.exists(path):
        raise Exception("{} path {} exists".format(description, path))

def ensure_unused_or_symlink(path, description):
    if os.path.islink(path):
        return
    ensure_unused_path(path, description)

class InstallPrecheckError(Exception):
    pass


def do_allnoconfig(conffile):
    conffile = os.path.expanduser(conffile)
    conffile = os.path.realpath(conffile)

    if not os.path.exists(conffile):
        raise Exception("{} does not exist!".format(conffile))

    nwenv = {key: os.environ[key] for key in os.environ}
    nwenv["KCONFIG_ALLCONFIG"] = conffile

    cmd = ["make", "allnoconfig"]
    os.execvpe(cmd[0], cmd, nwenv)

# Compute paths that we would end with by installing linux from
# some source directory where it has been built. Also perform
# a number of sanity checks, to avoid overwriting an existing
# kernel or config file or so on.
def install_precheck(srcdir, symlink_suffix=None):
    release_path = srcdir + "/include/config/kernel.release"
    try:
        version = line1_of_file_at(release_path)
    except FileNotFoundError:
        raise InstallPrecheckError("Could not find " + release_path)

    bzimage = srcdir + "/arch/x86/boot/bzImage"
    if not os.path.exists(srcdir + "/arch/x86/boot/bzImage"):
        raise InstallPrecheckError("could not find " + bzimage)

    config_path = "/boot/config-{}".format(version)
    linux_path = "/boot/linux-{}".format(version)
    initramfs_path = "/boot/initramfs-{}".format(version)
    modules_path = "/lib/modules/{}".format(version)

    ensure_unused_path(config_path, "config")
    ensure_unused_path(linux_path, "linux")
    ensure_unused_path(initramfs_path, "initramfs")
    ensure_unused_path(modules_path, "modules")

    if symlink_suffix != None:
        linux_sl = "/boot/linux-{}".format(symlink_suffix)
        initramfs_sl = "/boot/initramfs-{}".format(symlink_suffix)
        ensure_unused_or_symlink(linux_sl, "linux symlink")
        ensure_unused_or_symlink(initramfs_sl, "initramfs symlink")
    else:
        linux_sl = None
        initramfs_sl = None

    return {
        "srcdir": srcdir,
        "version": version,
        "config": config_path,
        "linux": linux_path,
        "modules": modules_path,
        "initramfs": initramfs_path,
        "linux_symlink": linux_sl,
        "initramfs_symlink": initramfs_sl,
        }

def install_cmds(srcdir=None,
                 version=None,
                 config=None,
                 linux=None,
                 modules=None,
                 initramfs=None,
                 linux_symlink=None,
                 initramfs_symlink=None):
    cp_config = ["cp", srcdir + "/.config", config]
    cp_bzimage = ["cp", srcdir + "/arch/x86/boot/bzImage", linux]
    modules_install = ["make", "modules_install"]
    dracut = ["dracut", initramfs, version]

    result = [cp_config,
              cp_bzimage,
              modules_install,
              dracut,
              ]

    if linux_symlink != None:
        old_link_rm = ["rm", "-f", linux_symlink]
        make_link = ["ln", "-s", os.path.basename(linux), linux_symlink]
        result.append(old_link_rm)
        result.append(make_link)

    if initramfs_symlink != None:
        old_link_rm = ["rm", "-f", initramfs_symlink]
        make_link = [
            "ln", "-s",
            os.path.basename(initramfs), initramfs_symlink
            ]
        result.append(old_link_rm)
        result.append(make_link)

    return result

def do_install(srcdir, symlink_suffix=None, dry_run=False):
    pathdata = install_precheck(srcdir, symlink_suffix=symlink_suffix)
    cmds = install_cmds(**pathdata)

    for cmd in cmds:
        if dry_run:
            print("+ " + " ".join(cmd))
        else:
            subprocess.call(cmd, cwd=srcdir)

def setup_allnoconfig_parser(p):
    p.add_argument("conffile", help="path to override file")

def setup_install_parser(p):
    p.add_argument("srcdir")
    p.add_argument("--symlink-with-suffix", dest="symlink_suffix")

def argument_parser():
    p = argparse.ArgumentParser()
    p.add_argument("--dry-run", action="store_true")
    sps = p.add_subparsers(dest="subcmd_name")

    anc_help = "run make allnoconfig with an override file"
    anc_parser = sps.add_parser("allnoconfig", help=anc_help)
    setup_allnoconfig_parser(anc_parser)

    install_help = "install kernel, modules, initramfs and symlinks"
    install_parser = sps.add_parser("install", help=install_help)
    setup_install_parser(install_parser)

    return p

def main(args):
    parsed_args = argument_parser().parse_args(args)

    if parsed_args.subcmd_name == "install":
        do_install(parsed_args.srcdir,
                   symlink_suffix=parsed_args.symlink_suffix,
                   dry_run=parsed_args.dry_run)
        return 0
    elif parsed_args.subcmd_name == "allnoconfig":
        do_allnoconfig(parsed_args.conffile)

    print("Somehow did end up without processing a subcommand")
    return 1

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
