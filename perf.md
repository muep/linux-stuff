Notes on using the perf tool for performance diagnostics.

# Requirements
On Fedora, dnf install perf

# Tasks

## Find out what is causing writes
There might be some filesystem-agnostic way too, but at least ext4
seems nice to inspect with these:

    perf record -a -g -e ext4:ext4_mark_inode_dirty <some command>
    perf record -a -g -e ext4:ext4_journal_start <some command>

Then view the results with this:

    perf report -s comm
