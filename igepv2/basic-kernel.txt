This repo has a file named igepv2_defconfig that can be used.

Complete set of steps:
 make ARCH=arm CROSS_COMPILE=armv7-unknown-linux-gnueabi- mrproper
 cp .../linux-stuff/igepv2/igepv2_defconfig .
 make ARCH=arm CROSS_COMPILE=armv7-unknown-linux-gnueabi- olddefconfig
 make ARCH=arm CROSS_COMPILE=armv7-unknown-linux-gnueabi- -j10 zImage dtbs modules

Now that the kernel has been built, copy these to the u-boot fileystem:
 arch/arm/boot/zImage
 arch/arm/boot/dts/omap3-igep0020.dtb
 uEnv.txt from this directory
