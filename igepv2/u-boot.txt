u-boot for IGEPv2:

At least v2014.04 from git://git.denx.de/u-boot.git is known to work
with these instructions:

make CROSS_COMPILE=armv7-unknown-linux-gnueabi- mrproper
make CROSS_COMPILE=armv7-unknown-linux-gnueabi- igep0020_config
make CROSS_COMPILE=armv7-unknown-linux-gnueabi-

From this, in the toplevel directory, we get these files:
 MLO
 System.map
 u-boot
 u-boot.bin
 u-boot.img
 u-boot.lds
 u-boot.map
 u-boot.srec

MLO is the thing that can be loaded directly with the OMAP boot ROM
from a FAT32 filesystem on an SD card.

The MLO will look for a u-boot.img file on the SD card. This will be
the full u-boot software. At the time of writing, no idea what the
other ones are useful for, except maybe for debugging.

U-boot, at least when configured for the IGEPv2, will look for a file
called uEnv.txt and load additions to its environment from that if it
is present. This directory contains an uEnv.txt that can be used with
the basic-kernel.txt guide.
