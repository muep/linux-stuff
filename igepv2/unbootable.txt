commit 9aa36dfd3d9a1c9e305e53f5c0915036c3941a3f

in the kernel.org kernel changes IGEPv2 device tree files so that
they describe an AM/DM37x processor based variant of IGEPv2. This
breaks the device tree for the OMAP3530 based IGEPv2.

At least with linux 3.14 series, it seems sufficient to revert that change.
