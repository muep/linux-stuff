#!/bin/sh
#
# A really crude hand-crafted installer for booting a CentOS userspace
# with a custom kernel image.

set -xe

if test $# -ne 2
then
    printf "usage: ${0} <path-to-kernel> <path-to-output-disk-image>\n"
    exit 1
fi

if ! command -v kpartx > /dev/null 2>&1
then
    echo "kpartx is missing"
    exit 3
fi

diskfile=$(readlink -f "${2}")
linuxfile=$(readlink -f "${1}")
if ! test -e "${linuxfile}"
then
    echo "${linuxfile} does not exist"
    exit 2
fi

dd if=/dev/zero bs=1M count=1000 of="${diskfile}" conv=sparse

fdisk "${diskfile}" << EOF
o
n
p
1


p
w
EOF

kpartx -a -v "${diskfile}"

loopfile=$(losetup -l | awk '$6 == "'"${diskfile}"'" { print $1 }')
part1=/dev/mapper/$(basename "${loopfile}")p1

mkfs.ext4 "${part1}"

mkdir -p /mnt/sysroot
mount "${part1}" /mnt/sysroot

yum -y --nogpg --releasever=7 \
    --disablerepo='*' --enablerepo=base \
    --installroot=/mnt/sysroot install \
    centos-release \
    dhclient \
    grub2 \
    iproute \
    iputils \
    less \
    passwd \
    systemd \
    yum \
    vim-minimal

mount -t proc proc /mnt/sysroot/proc
mount -t sysfs sys /mnt/sysroot/sys
mount -o bind /dev /mnt/sysroot/dev

cp "${linuxfile}" /mnt/sysroot/boot/linux
cat > /mnt/sysroot/boot/grub2/grub.cfg <<'EOF'
set timeout=0
set default=0

menuentry "Linux" {
    set root=(hd0,1)
    linux /boot/linux root=/dev/sda1 console=ttyS0
}
EOF

chroot /mnt/sysroot /bin/bash <<EOF
set -x
passwd -d root
grub2-install ${loopfile}
EOF

umount /mnt/sysroot/proc
umount /mnt/sysroot/sys
umount /mnt/sysroot/dev
umount /mnt/sysroot

kpartx -d -v "${diskfile}"
